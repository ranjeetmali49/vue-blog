import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import 'bootstrap/dist/css/bootstrap.css'
import 'jquery/dist/jquery.min'
import 'popper.js/dist/popper.min'
import 'bootstrap/dist/js/bootstrap.min'
import './assets/mystyle.css'
import {library} from '@fortawesome/fontawesome-svg-core'
import {faPlus, faTrash, faEdit, faEye} from '@fortawesome/free-solid-svg-icons'
import {FontAwesomeIcon} from '@fortawesome/vue-fontawesome'

library.add(faPlus, faTrash, faEdit, faEye);
Vue.component('font-awesome-icon', FontAwesomeIcon);

Vue.config.productionTip = false;

if (localStorage.getItem('token')) {
    store.dispatch('authSuccess');
}

new Vue({
    router,
    store,
    render: h => h(App)
}).$mount('#app');
