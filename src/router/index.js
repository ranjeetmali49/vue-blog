import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../components/Home.vue'
import About from '../components/About.vue'
import Login from "../components/Login";
import Register from "../components/Register";
import Dashboard from "../components/private/Dashboard";
import CategoryList from "../components/private/category/CategoryList";
import CategoryCreate from "../components/private/category/CategoryCreate";
import CategoryEdit from "../components/private/category/CategoryEdit";
import PostList from "../components/private/post/PostList";
import PostCreate from "../components/private/post/PostCreate";
import PostEdit from "../components/private/post/PostEdit";
import PostView from "../components/PostView";
import Profile from "../components/private/Profile";
import NotFound from "../components/NotFound";
import store from "../store";

Vue.use(VueRouter);

const routes = [
    {
        path: '/',
        name: 'home',
        component: Home
    },
    {
        path: '/about',
        name: 'about',
        component: About
    },
    {
        path: '/login',
        name: 'login',
        component: Login
    },
    {
        path: '/register',
        name: 'register',
        component: Register
    },
    {
        path: '/dashboard',
        name: 'dashboard',
        component: Dashboard,
        meta: {
            require_auth: true
        }
    },
    {
        path: '/category',
        name: 'category',
        component: CategoryList,
        meta: {
            require_auth: true
        }
    },
    {
        path: '/category/create',
        name: 'category_create',
        component: CategoryCreate,
        meta: {
            require_auth: true
        }
    },
    {
        path: '/category/:id/edit',
        name: 'category_edit',
        component: CategoryEdit,
        meta: {
            require_auth: true
        }
    },
    {
        path: '/post',
        name: 'post',
        component: PostList,
        meta: {
            require_auth: true
        }
    },
    {
        path: '/post/create',
        name: 'post_create',
        component: PostCreate,
        meta: {
            require_auth: true
        }
    },
    {
        path: '/post/:id/edit',
        name: 'post_edit',
        component: PostEdit,
        meta: {
            require_auth: true
        }
    },
    {
        path: '/post/:id',
        name: 'post_view',
        component: PostView,
        meta: {
            require_auth: true
        }
    },
    {
        path: '/profile',
        name: 'profile',
        component: Profile,
        meta: {
            require_auth: true
        }
    },
    {
        path: '*',
        name: 'not_found',
        component: NotFound
    }
];

const router = new VueRouter({
    mode: 'history',
    base: process.env.BASE_URL,
    routes,
});


router.beforeEach((to, from, next) => {
    if (to.meta.require_auth) {
        if (store.getters.isAuthenticated) {
            next();
        } else {
            next('/login');
        }
    }
    next();
});

export default router
