import Vue from 'vue'
import Vuex from 'vuex'
import {todo} from "./todo";
import {auth} from "./module/auth";
import {categories} from "./module/categories";

Vue.use(Vuex)

export default new Vuex.Store({
    modules: {
        auth,
        categories,
        todo
    }
})