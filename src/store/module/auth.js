import {requestGetUser} from "../../api/api";

export const auth = {
    state: {
        user: null
    },
    mutations: {
        SET_AUTH: (state, user) => {
            state.user = user;
        },
        AUTH_LOGOUT: (state) => {
            state.user = null;
        }
    },
    actions: {
        authSuccess(context) {
            context.commit('SET_AUTH', true);
            requestGetUser().then(response => {
                console.log(response.data);
                context.commit('SET_AUTH', response.data);
            }).catch(() => {
                context.commit('AUTH_LOGOUT')
            });
        },
        authLogout(context) {
            localStorage.removeItem('token');
            context.commit('AUTH_LOGOUT')
        }
    },
    getters: {
        isAuthenticated: state => {
            return !!state.user;
        }
    }
};