export const categories = {
    state: {
        categories: []
    },
    mutations: {
        SET_CATEGORIES: (state, categories) => {
            state.categories = categories;
        },
        ADD_CATEGORY: (state, category) => {
            state.categories.push(category);
        },
        REMOVE_CATEGORY: (state, category_id) => {
            state.categories = state.categories.filter((cat) => cat.id !== category_id);
        },
        UPDATE_CATEGORY: (state, {category_id, category_data}) => {
            let categoryIndex = state.categories.findIndex(data => {
                return data.id === category_id;
            });
            state.categories[categoryIndex] = category_data;
        }
    },
    actions: {
        setCategories(context, categories) {
            context.commit('SET_CATEGORIES', categories);
        },
        addCategory(context, category) {
            context.commit('ADD_CATEGORY', category);
        },
        removeCategory(context, category_id) {
            context.commit('REMOVE_CATEGORY', category_id);
        },
        updateCategory(context, data) {
            context.commit('UPDATE_CATEGORY', data);
        }
    }
};