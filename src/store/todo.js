export const todo = {
    state: {
        todos: ['first','sec']
    },
    mutations: {
        ADD_TODO: (state, todo) => {
            state.todos.push(todo)
        },
        REMOVE_TODO: (state, index) => {
            state.todos.splice(index, 1);
        }
    },
    actions: {
        addTodo(context, todo) {
            context.commit('ADD_TODO', todo)
        },
        removeTodo(context, index) {
            context.commit('REMOVE_TODO', index)
        }
    },
    getters: {

    }
};